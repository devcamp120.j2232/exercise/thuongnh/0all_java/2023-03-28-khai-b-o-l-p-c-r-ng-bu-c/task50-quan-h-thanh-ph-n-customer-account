import javax.swing.tree.FixedHeightLayoutCache;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;

    /**
     * 
     * @param id
     * @param customer
     * @param balance
     */

    public Account(int id, Customer customer, double balance) {
        this.customer = customer;
        this.id = id;
        this.balance = balance;
    }

   /**
    * 
    * @param id
    * @param customer
    */

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", customer=" + customer + ", balance=" +  (Math.round(balance * 100.0) / 100.0) + "]";
    }

    public String getCustomerName() {
        return customer.getName();
    }
    // z thêm tiền vào số dư
    public Account  deposit(double amount) {
        this.balance += amount;
        return this;
    }

    public Account withdrow(double amount){
        if(amount <= balance) {
            // trừ tiền vào số dư 
            System.out.println("đã trừ vào số dư");
            this.balance -= amount;
        }
        else {
            System.out.println("amount to balance exceeds the current balance!");
        }
        return this;
    }

    

}
