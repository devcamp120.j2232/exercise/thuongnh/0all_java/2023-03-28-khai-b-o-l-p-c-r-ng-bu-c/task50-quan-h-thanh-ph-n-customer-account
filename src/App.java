public class App {
    public static void main(String[] args) throws Exception {

        // yc 4 tạo hai đối tượng khách hàng
        Customer customer1 = new Customer(10220, "John", 20);
        Customer customer2 = new Customer(19228, "thuong", 70);

        System.out.println("customer1  " + customer1);
        System.out.println("customer2  " + customer2);

        // yêu cầu 5 . tạo hai đối tượng tài khoản

        Account account1 = new Account(1, customer1, 1000);
        Account account2 = new Account(2, customer2, 5000);


        System.out.println("account1  " + account1);
        System.out.println("account2  " + account2);

        account2.deposit(1000);
        account1.withdrow(5000);

        System.out.println("account2  lần 2:  " + account2);
        System.out.println("account1  lần 2:   " + account1);

    }

}
